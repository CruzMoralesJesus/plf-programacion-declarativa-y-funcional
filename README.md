# Mapas conceptuales<br>Programación Declarativa y Funcional
## 1. Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
```plantuml
@startmindmap

<style>
 node{
  Padding 10
  Margin 4
  HorizontalAlignment center
  LineColor #D8B5F9
  LineThickness 1.5
  BackgroundColor #D8B5F9
  RoundCorner 40
  MaximumWidth 300
 }

 arrow{
  LineColor #58047F
 }

 title{
  FontSize 18
 }

 header{
  FontSize 14
 }
 </style>

caption Mapa conceptual 1/2
title Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
header
Cruz Morales Jesús
endheader

 * <b>Programación\n<b>Declarativa
  *_ resume las ideas de
   *_ más
    * Parte\ncreativa
     *_ consiste en
      * Algoritmos
      * Formas de resolver el problema
      * Formas de ahorrar 
       * Tiempo
       * Memoria
     *_ dejando las
      * Tareas\nrutinarias
       *_ al
        * Compilador
         *_ abandonando\nasí la
          * Gestión de\nmemoria
           *_ y solo darle 
            * Explicaciones
             *_ de como\nson los
              * Algoritmos 
               *_ para
                * Resolver el\nproblema
   *_ menos
    * Parte\nburocrática
     *_ es la
      * Gestión\ndetallada 
       *_ de 
        * Memoria
         *_ para seguir\nuna
          * Secuencia\nde ordenes
           *_ que lleven\na una
            * Solución
  *_ su importancia
   * Fundamental
    *_ para aprender a
     * Programar\nbien
      *_ de forma
       * Sencilla y\nplacentera
    *_ supone el
     * Principio del\nconocimiento
      *_ de las
       * Características\nmás relevantes
        *_ de los
         * Lenguajes\nactuales
      *_ técnicas de
       * Programación\nbásica
        *_ de estos
         * Lenguajes
  *_ ventajas
   * Liberación de\nasignaciones
    *_ la
     * Asignación
      *_ esta modifica el\nestado de la
       * Memoria
        *_ y el
         * Ordenador
          *_ almacena
           * Paso a paso\ncada instrucción
    *_ evita un
     * Cuello de botella\nintelectual
   * Uso de otros\nrecursos expresivos
    *_ para especificar
     * Programas a\nnivel más alto
      *_ y cercano al\npensar del
       * Programador
    *_ según el
     * Tipo de lenguaje o\nrecursos expresivos
      *_ surgen dos\nvariantes principales
       * Programación\nfuncional
        *_ utiliza
         * Lenguaje\nmatemático
          *_ para\ndefinir
           * Funciones
            *_ se asimilan\na
             * Programas
              *_ ya que una\nlos
               * Datos de\nentrada
                *_ equivalentes a 
                 * Argumentos
               * Datos de\nsalida
                *_ equivalentes al
                 * Resultado de\nla función
          *_ en especial\nel
           * Razonamiento\necuacional
        *_ ventajas
         * Funciones de\norden superior
          *_ se puede\nentender como
           * Funciones que actuán\nsobre funciones
            *_ en una
             * Sucesión\ninfinita
              *_ ya que no hay\ndistinción entre
               * Datos y\nprograma
         * Evaluación\nperezosa
          *_ consiste en
           * Computar o\nevaluar
            *_ solo las
             * Ecuaciones\nnecesarias
              *_ para hacer
               * Cálculos\nposteriores
         * Tipos de datos\ninfinitos
          *_ por ejemplo\nse puede decir
           * Serie de números\nde fibonacci
       * Programación\nlógica
        *_ acude a la
         * Lógica de\npredicados
          *_ de
           * Primer\norden
          *_ los
           * Predicados
            *_ son
             * Relaciones entre\nobjetos
              *_ que se\nestán
               * Definiendo
              *_ estas relaciones no\nestablecen un
               * Orden
                *_ entre
                 * Argumentos de\nentrada y salida
        *_ basado en el\nmodelo de
         * Demostración lógica\ny automática
        *_ utiliza como
         * Expresiones
          *_ de los
           * Programas
            * Axiomas
            * Reglas de\ninferencia
             *_ igual que en
              * Sistemas\nlógicos
        *_ forma de realizar\nel computo
         * Intérprete
          *_ dado un\nconjunto de 
           * Relaciones y\npredicados
            *_ opera\nmediante
             * Algoritmos de\nresolución
              *_ intenta demostrar\npredicados dentro del
               * Sistema
            *_ el
             * Compilador
              *_ actua\ncomo
               * Motor de\ninferencia
                *_ sobre estos,\nes decir
                 * Razona
                  *_ para\ndar
                   * Respuesta a\nla pregunta
   * Programas
    *_ más fáciles de
     * Realizar, modificar\ny depurar
      *_ ya que es fácil
       * Comprobar que\nestá haciendo
        *_ aunque\nsea un
         * Programa\najeno
        *_ presenta los\ndetalles con más
         * Perspectiva
   * Menor tiempo\nde desarrollo
    *_ en comparación a un
     * Lenguaje\nimperativo
  *_ Lenguajes\nrecomendados
   * Orientados a la\nInteligencia Artificial
    * Lisp
     *_ relacionado a la
      * Programación funcional
    * Prolog
     *_ estandar que implementa\nel concepto de
      * Programación lógica
   * Haskell
    *_ para\nprogramación
     * Funcional
    *_ con los compiladores
     * Gofer
     * Guix
  *_ libros\nrecomendados
   * Programación funcional
    *_ de
     * Jeroen Fokker
   * Lenguajes de programación:\nconceptos y constructores
    *_ de
     * Ravi Sethi
@endmindmap
```
## 2. Lenguaje de Programación Funcional (2015)
```plantuml
@startmindmap

<style>
 node {
  Padding 8
  Margin 4
  HorizontalAlignment center
  LineColor #D5ACE8
  LineThickness 1.5
  BackgroundColor #D5ACE8
  RoundCorner 40
  MaximumWidth 300
 }

 arrow{
  LineColor #58047F
 }

 title{
  FontSize 18
 }

 header{
  FontSize 14
 }
</style>

caption Mapa conceptual 2/2
title Lenguaje de Programación Funcional (2015)
header
Cruz Morales Jesús
endheader

 * <b>Paradigma de\n<b>programación
  *_ es un
   * Modelo de computación
    *_ en el\nque los
     * Lenguajes
      *_ dotan de
       * Semántica
        *_ a los
         * Programas
  *_ los iniciales\nbasados en el
   * Modelo de\nvon Neumman
    *_ este modelo\ndice que
     * Los programas
      *_ deben ser
       * Almacenados
        *_ en la misma
         * Máquina
          *_ antes\nde ser
           * Ejecutado
    *_ surge el
     * Paradigma\nimperativo
      *_ la
       * Ejecución
        *_ siguiendo el modelo,\nconsistirá en una
         * Serie de\ninstrucciones
          *_ que se\nejecutará
           * Secuencialmente
      *_ la modificación\nde
       * Secuencias
        *_ se podría\nsegún el
         * Estado del\ncómputo
          *_ es una 
           * Zona de\nmemoria
            *_ a la que se accede\nmediante una
             * Serie de\nvariables
              *_ y que en un\nmomento dado
               * Almacena\nel valor
                *_ de todas\nlas
                 * Variables
  *_ el
   * Orientado\na Objetos
    *_ consiste en\npequeños
     * Trozos de código
      *_ que
       * Interactúan\nentre sí
        *_ siguen\nsiendo
         * Instrucciones\nsecuenciales
  *_ de
   * Programación\nlógico
    *_ basado\nen la
     * Lógica\nsimbólica
    *_ un
     * Programa
      *_ se forma\nmediante un
       * Conjunto de\nsentencias
        *_ que definen\nlo que es
         * Verdad y\nconocido
          *_ para el programa\ncon respecto de un
           * Problema
            *_ mediante
             * Inferencia\nlógica
  *_ de
   * Programación\nfuncional
    *_ ¿Cómo surgió?
     * Lambda\ncálculo
      *_ surgió en
       * 1930
        *_ por
         * Church y Kleene
      *_ pensado\ncomo un
       * Sistema
        *_ para estudiar\nel concepto de
         * Función, aplicación de\nfunción y recursividad
          *_ obtuvieron un
           * Sistema\ncomputacional
            *_ equivalente\nal de
             * Neumman
            *_ este dice que, todo\nlo que se puede
             * Hacer\nen uno
              *_ se puede
               * Hacer\nen otro
      *_ seguido por
       * Haskell
        *_ desarrolló\nla
         * Lógica\ncombinatoria
          *_ una
           * Variante de\nlambda cálculo
            *_ esto dió\nlos
             * Cimientos
              *_ a
               * Peter\nLandin
                *_ y le permite\nmodelar un
                 * Lenguaje de\nprogramación
                  *_ esto\nsentó las
                   * Bases de la\nprogramación\nfuncional
    *_ su concepción
     * Pura matemática
      *_ todo gira alrededor de
       * Funciones
    *_ sus\ncaracterísticas
     *_ se pierden el
      * Concepto fundamental de asignación
      * Concepto de variable
       *_ como una
        * Zona de memoria
         *_ que se puede
          * Modificar
           *_ a lo largo del
            * Cómputo
     * Desaparecen los bucles
      *_ aparece la
       * Recursión
        *_ estas son
         * Funciones
          *_ que hacen 
           * Referencia así misma
     * Variables
      *_ existen\npero para
       * Referirse
        *_ a los
         * Parámetros\nde entrada
          *_ de las
           * Funciones
     * Constantes
      *_ equiparan a
       * Funciones\nconstantes
        *_ es decir
         * Funciones\nsin parámetro 
     * Transparencia\nreferencial
      *_ es una
       * Función
        *_ que recibe\nlos mismos
         * Parámetros\nde entrada
          *_ siempre\ndevolverá el
           * Mismo valor
     * El resultado
      *_ de las\noperaciones son
       * Independientes
        *_ del
         * Orden en que\nse realicen
        *_ esto permite
         * Paralelizar\nsin problema
          *_ hablando en\nun entorno
           * Multiprocesador
     * Currificación
      *_ es una función\nque tiene
       * Diferentes parámetros\nde entrada
        *_ no solo devuelve\nuna salida, devuelve
         * Diferentes\nfunciones
        *_ si no utiliza algún\nparámetro se considera
         * Aplicación parcial
          *_ si se utilizan todas
           * Aplicación total
    *_ la evalución\nde funciones
     * Evalución impaciente\no estricta
      *_ consiste en evaluar\nfunciones de
       * Adentro hacia\nafuera
      *_ es
       * Fácil de\nimplementar
        *_ pero presenta un\ninconveniente si
         * No se evalua\nuna expresión
          *_ en un
           * Tiempo\nfinito
            *_ puede que el\ncálculo
             * No se\ninterrumpa
              *_ o producirse\nun
               * Error
     * Evalucación\nno estricta
      *_ consiste en evaluar\nfunciones de
       * Afuera hacia\nadentro
      *_ es
       * Más difícil\nde implementar
        *_ ya que
         * No necesita\nconocer el valor
          *_ de uno\nde sus
           * Parámetros
            *_ para devolver\nun
             * Resultado
      *_ en el
       * Paradigma\nimperativo
        *_ a esto se\nle conoce como
         * Evalución en cortocircuito\no de McCarthy
      *_ conocido también\ncomo
       * Evaluación\nperezosa
        *_ hace
         * Evaluación de\nlos parámetros
          *_ hasta que\nson
           * Necesitados
            *_ por lo que hace
             * Paso por\nnecesidad
        *_ usa la
         * Memoización
          *_ consiste en
           * Almacenar
            *_ el 
             * Valor de\nuna expresión
              *_ cuya
               * Evaluación
                *_ ya ha sido
                 * Realizada
    *_ sus ventajas
     * Uso de funciones\nde orden superior
      *_ esta es una
       * Función que recibe otra
        *_ como
         * Parámetro de\nentrada
          *_ la salida es
           * Otra función
     * Realizar de prototipos rápidamente
     * Eficiencia
      *_ con respecto a la
       * Programación
        *_ se refiere a\ncuan rápido se
         * Escribe un\nprograma
          *_ por consiguiente\nes más
           * Rápido y corto\nde escribir
     * Se aplica a\ncualquier campo
    *_ su desventaja
     * Mayor coste computacional
     * Puede tardar más
      *_ que programas\nen
       * Java o C
  *_ libro recomendado
   * Teoría de los lenguajes de programación
    *_ de la editorial
     * Ramón Areces
@endmindmap
```
